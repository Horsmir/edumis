#include <QtGui/QMessageBox>
#include <QtCore/QSettings>
#include <QtGui/QFileDialog>
#include "edumis.h"
#include "logindlg.h"
#include "hiswindow.h"
#include "datamanager.h"
#include "setupdialog.h"

edumis::edumis(QWidget *parent, Qt::WindowFlags flags): QMainWindow(parent, flags), ui(new Ui::CenterWindow), dbFileName(QString())
{
    ui->setupUi(this);
    lDlg = new LoginDlg(this);
    setupDialog = new SetupDialog(this);
    createList();

    dataManager = new DataManager(this);
    
    settings = new QSettings("../config/edumis.conf", QSettings::NativeFormat, this); // TODO: заменить на: QSettings(QSettings::NativeFormat, QSettings::UserScope, "EduMIS", "edumis", this)
    readSettings();
}

edumis::~edumis()
{
    delete ui;
}

void edumis::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);    
}

void edumis::on_listWidget_itemClicked(QListWidgetItem *current)
{
    switch(ui->listWidget->row(current))
    {
    case 0:

        openDataBase();
	if(lDlg->exec() != QDialog::Accepted)
        {
            dataManager->disconnectDb();
            break;
        }

        historyWindow = new HisWindow(this);
        historyWindow->setDb(dataManager->getDatabase());
        historyWindow->setUserId(dataManager->getUserId());
        historyWindow->show();
        break;
	
    case 1:
	setupDialog->setDbFileName(dbFileName);
	if(setupDialog->exec() == QDialog::Accepted)
	{
	    dbFileName = setupDialog->getDbFileName();
	    dataManager->disconnectDb();
	    if(!dataManager->connectDb(dbFileName))
		QMessageBox::critical(this, trUtf8("Соединение с базой данных"), dataManager->getLastError());
	}

    default:
        break;
    }
}

void edumis::createList()
{
    QListWidgetItem *hisButton = new QListWidgetItem(ui->listWidget);
    hisButton->setIcon(QIcon(":/icons/case_history.png"));
    hisButton->setText(trUtf8("Истории болезни"));
    hisButton->setTextAlignment(Qt::AlignHCenter);
    hisButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    
    QListWidgetItem *setupButton = new QListWidgetItem(ui->listWidget);
    setupButton->setIcon(QIcon(":/icons/setup.png"));
    setupButton->setText(trUtf8("Параметры"));
    setupButton->setTextAlignment(Qt::AlignCenter);
    setupButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
}

void edumis::readSettings()
{
    dbFileName = settings->value("General/DbFileName", QString()).toString();
}

void edumis::writeSettings()
{
    settings->setValue("General/DbFileName", dbFileName);
}

void edumis::closeEvent(QCloseEvent *event)
{
    writeSettings();
    QWidget::closeEvent(event);
}

void edumis::getDbFileName()
{
    QFileDialog::Options options;
    options |= QFileDialog::DontUseNativeDialog;
    QString selectedFilter;
     QString fileName = QFileDialog::getOpenFileName(this, trUtf8("Файл базы данных"), QDir::homePath(), trUtf8("Все файлы (*);;Файлы SQLite (*.sqlite)"), &selectedFilter, options);
     if (!fileName.isEmpty())
         dbFileName = fileName;
}

void edumis::openDataBase()
{
    if(dbFileName.isEmpty())
	getDbFileName();

    if(!dataManager->connectDb(dbFileName))
    {
        QMessageBox::critical(this, trUtf8("Соединение с базой данных"), dataManager->getLastError());
        close();
    }

    lDlg->setDataManager(dataManager);
}

#include "edumis.moc"
