/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LOGINDLG_H
#define LOGINDLG_H

#include <QtGui/QDialog>
#include "ui_logindlg.h"

class DataManager;

namespace Ui
{
    class LoginDialog;
};

class LoginDlg : public QDialog
{
    Q_OBJECT
public:
    explicit LoginDlg(QWidget *parent = 0, Qt::WindowFlags f = 0);
    virtual ~LoginDlg();

    bool checkLogin();
    void setDataManager(DataManager *dataManager);

public slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::LoginDialog *ui;
    DataManager *dataManager;
};

#endif // LOGINDLG_H
