/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DATAHISTORYMANAGER_H
#define DATAHISTORYMANAGER_H

#include <QtCore/QObject>

class QSqlDatabase;
class QStringList;
class QSqlQuery;
class QVariant;

class DataHistoryManager : public QObject
{
    Q_OBJECT
public:
    explicit DataHistoryManager(QObject *parent = 0);
    virtual ~DataHistoryManager();

    void setDatabase(QSqlDatabase *db);
    void setUserId(quint32 id);
    void setHistoryId(quint32 id);

    QList<QList<QVariant> > getHistoryList();
    bool addNewHistory();

    QList<QVariant> getPasportData() const; // получить паспортные данные
    QStringList getMedicalHisroryData() const; // получить анамнез заболевания
    QStringList getAnamnesisLifeData() const; // получить анамнез жизни
    QString getObjectiveStatusData() const; // получить объективный статус
    QStringList getOrganSystemsData() const; // получить описание состояния систем органов
    QString getPilotDiagnosisData() const; // получить предварительный диагноз и его обоснование
    QString getLaboratoryResearchData() const; // получить лабораторные данные
    QStringList getFinishDiagnosisData() const; // получить окончательный диагноз и его обоснование
    QStringList getTreatmentData() const; // получить лечение
    QString getProphylaxisData() const; // получить профилактику
    QString getPrognosisData() const; // получить прогноз и его обоснование
    QString getEpicrisisData() const; // получить эпикриз

    bool setPasportData(QList<QVariant> data); // сохранить паспортные данные
    bool setMedicalHisroryData(QStringList data); // сохранить анамнез заболевания
    bool setAnamnesisLifeData(QStringList data); // сохранить анамнез жизни
    bool setObjectiveStatusData(const QString &data); // сохранить объективный статус
    bool setOrganSystemsData(QStringList data); // сохранить описание состояния систем органов
    bool setPilotDiagnosisData(const QString &data); // сохранить предварительный диагноз и его обоснование
    bool setLaboratoryResearchData(const QString &data); // сохранить лабораторные данные
    bool setFinishDiagnosisData(QStringList data); // сохранить окончательный диагноз и его обоснование
    bool setTreatmentData(QStringList data); // сохранить лечение
    bool setProphylaxisData(const QString &data); // сохранить профилактику
    bool setPrognosisData(const QString &data); // сохранить прогноз и его обоснование
    bool setEpicrisisData(const QString &data); // сохранить эпикриз

    QString getLastErrorString() const;

private:
    QSqlDatabase *db;
    QSqlQuery *query;
    quint32 userId;
    quint32 historyId;
    QString lastErrorString;
};

#endif // DATAHISTORYMANAGER_H
