#EduMIS 0.1.0

**EduMIS** - Учебная медицинская информационная система. Предназначена для обретения практических навыков студентами медицинских институтов и колледжей в обработке медицинской информации. Написана на Qt-C++.
Кросплатформенная.

> **EduMIS** является свободным программным обеспечением. Вы можете распространять и/или модифицировать её согласно условиям Стандартной Общественной Лицензии GNU, опубликованной Фондом Свободного Программного  Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии.
>
> Эта программа распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную Общественную Лицензию GNU для получения дополнительной информации.
>
> Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе с программой. В случае её отсутствия, посмотрите [www.gnu.org/licenses/](http://www.gnu.org/licenses/). 

Сайт программы: [bitbucket.org/Horsmir/edumis](https://bitbucket.org/Horsmir/edumis).

Copyright© 2013 Роман Браун

Icons: [www.smashingmagazine.com](http://www.smashingmagazine.com/2010/02/15/free-medical-icons-set-60-icons/)

Email: <firdragon76@gmail.com>

***

##Установка EduMIS:

###ОС GNU/Linux:

1. В директории с исходным кодом создать каталог сборки
~~~~
$ mkdir -v build
~~~~

1. перейти в каталог сборки
~~~~
$ cd build
~~~~

1. Сборка
~~~~
$ cmake -DCMAKE_INSTALL_PREFIX=<install_dir_path> -DCMAKE_BUILD_TYPE=Release ..
$ make edumis
~~~~

<install_dir_path> - префикс пути установки бинарных файлов (по умолчанию = /usr)

1. Установка
~~~~
# make install
~~~~

##Установка менеджера базы данных EduMIS:

###ОС GNU/Linux:

2. В директории с исходным кодом создать каталог сборки
~~~~
$ mkdir -v build
~~~~

2. перейти в каталог сборки
~~~~
$ cd build
~~~~

2. Сборка
~~~~
$ cmake -DCMAKE_INSTALL_PREFIX=<install_dir_path> -DCMAKE_BUILD_TYPE=Release ..
$ make confedumis
~~~~
<install_dir_path> - префикс пути установки бинарных файлов (по умолчанию = /usr)

2. Установка
~~~~
# make install
~~~~
