/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QtGui/QFileDialog>
#include "setupdialog.h"
#include "ui_setupdlg.h"

SetupDialog::SetupDialog(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::SetupDlg), dbFileName(QString())
{
    ui->setupUi(this);
}

SetupDialog::~SetupDialog()
{
    delete ui;
}

QString SetupDialog::getDbFileName() const
{
    return dbFileName;
}

void SetupDialog::setDbFileName(const QString &dbFileName)
{
    this->dbFileName = dbFileName;
    ui->leDbFileName->setText(this->dbFileName);
}

void SetupDialog::on_openButton_clicked()
{
    QFileDialog::Options options;
    options |= QFileDialog::DontUseNativeDialog;
    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName(this, trUtf8("Файл базы данных"), dbFileName, trUtf8("Все файлы (*);;Файлы SQLite (*.sqlite)"), &selectedFilter, options);
    if (!fileName.isEmpty())
    {
	dbFileName = fileName;
	ui->leDbFileName->setText(dbFileName);
    }
}

#include "setupdialog.moc"