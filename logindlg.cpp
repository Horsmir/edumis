/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QMessageBox>
#include "logindlg.h"
#include "datamanager.h"

LoginDlg::LoginDlg(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
}

LoginDlg::~LoginDlg()
{
    delete ui;
}

void LoginDlg::on_okButton_clicked()
{
    if(checkLogin())
    {
        ui->leLogin->clear();
        ui->lePassword->clear();
        accept();
    }
    else
    {
        ui->leLogin->clear();
        ui->lePassword->clear();
        QMessageBox::critical(this, trUtf8("Регистрация"), trUtf8("Не верный логин или пароль. Попробуйте ввести ещё раз или нажмите \"Отмена\""));
        return;
    }
}

bool LoginDlg::checkLogin()
{
    if(dataManager->checkUser(ui->leLogin->text(), ui->lePassword->text()))
        return true;

    return false;
}

void LoginDlg::on_cancelButton_clicked()
{
    ui->leLogin->clear();
    ui->lePassword->clear();
    emit reject();
}

void LoginDlg::setDataManager(DataManager *dataManager)
{
    this->dataManager = dataManager;
}

#include "logindlg.moc"
