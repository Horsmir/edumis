/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtCore/QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtCore/QStringList>
#include <QtCore/QDate>
#include "datahistorymanager.h"

DataHistoryManager::DataHistoryManager(QObject *parent): QObject(parent), userId(0), historyId(0), lastErrorString(QString())
{

}

DataHistoryManager::~DataHistoryManager()
{
    delete query;
}

void DataHistoryManager::setDatabase(QSqlDatabase *db)
{
    this->db = db;
    query = new QSqlQuery;
}

void DataHistoryManager::setUserId(quint32 id)
{
    userId = id;
}

QList< QList< QVariant > > DataHistoryManager::getHistoryList()
{
    QList< QList< QVariant > > res;

    bool ok = query->exec(QString("SELECT id, fio FROM case_histories WHERE user_id=%1").arg(userId));

    while(query->next())
    {
        QList<QVariant> temp;
        temp.append(query->value(0));
        temp.append(query->value(1));
        res.append(temp);
    }

    if(!ok)
        lastErrorString = query->lastError().text();

    return res;
}

bool DataHistoryManager::addNewHistory()
{
    bool ok = query->exec(QString("INSERT INTO case_histories(user_id) VALUES (%1)").arg(userId));

    if(ok)
    {
        query->exec(QString("SELECT id FROM case_histories WHERE user_id=%1").arg(userId));
        query->last();
        historyId = query->value(0).toUInt();
        return true;
    }

    if(!ok)
        lastErrorString = query->lastError().text();

    return false;
}

QList< QVariant > DataHistoryManager::getPasportData() const
{
    QList<QVariant> ret;

    query->exec(QString("SELECT fio, dob, place_empl, delivery_date, assign, diagnosis, bedroom_diagnosis, address FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        ret.append(query->value(0));
        ret.append(query->value(1));
        ret.append(query->value(2));
        ret.append(query->value(3));
        ret.append(query->value(4));
        ret.append(query->value(5));
        ret.append(query->value(6));
        ret.append(query->value(7));
    }

    return ret;
}

void DataHistoryManager::setHistoryId(quint32 id)
{
    historyId = id;
}

QStringList DataHistoryManager::getAnamnesisLifeData() const
{
    QStringList res;

    query->exec(QString("SELECT biography, labour_anamnesis, food, epidemiological_anamnesis, bad_habits, family_anamnesis, allergic_anamnesis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        for(int i = 0; i < 7; i++)
            res.append(query->value(i).toString());
    }

    return res;
}

QString DataHistoryManager::getEpicrisisData() const
{
    QString res;

    query->exec(QString("SELECT epicrisis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QStringList DataHistoryManager::getFinishDiagnosisData() const
{
    QStringList res;

    query->exec(QString("SELECT finish_diagnosis_bases, finish_diagnosis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        for(int i = 0; i < 2; i++)
            res.append(query->value(i).toString());
    }

    return res;
}

QString DataHistoryManager::getLaboratoryResearchData() const
{
    QString res;

    query->exec(QString("SELECT laboratory_research FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QStringList DataHistoryManager::getMedicalHisroryData() const
{
    QStringList res;

    query->exec(QString("SELECT ill_complaints, anamnesis_sickness FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        for(int i = 0; i < 2; i++)
            res.append(query->value(i).toString());
    }

    return res;
}

QString DataHistoryManager::getObjectiveStatusData() const
{
    QString res;

    query->exec(QString("SELECT objective_status FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QStringList DataHistoryManager::getOrganSystemsData() const
{
    QStringList res;

    query->exec(QString("SELECT respiration_organs, digestion_organs, urination_organs FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        for(int i = 0; i < 3; i++)
            res.append(query->value(i).toString());
    }

    return res;
}

QString DataHistoryManager::getPilotDiagnosisData() const
{
    QString res;

    query->exec(QString("SELECT pilot_diagnosis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QString DataHistoryManager::getPrognosisData() const
{
    QString res;

    query->exec(QString("SELECT prognosis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QString DataHistoryManager::getProphylaxisData() const
{
    QString res;

    query->exec(QString("SELECT prophylaxis FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        res = query->value(0).toString();
    }

    return res;
}

QStringList DataHistoryManager::getTreatmentData() const
{
    QStringList res;

    query->exec(QString("SELECT regimen, diet, medicament_treatment, physiotherapy, physiotherapy_exercises, sanatoria, operative, dispensary FROM case_histories WHERE id=%1").arg(historyId));

    if(query->next())
    {
        for(int i = 0; i < 8; i++)
            res.append(query->value(i).toString());
    }

    return res;
}

bool DataHistoryManager::setAnamnesisLifeData(QStringList data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET biography='%1', labour_anamnesis='%2', food='%3', epidemiological_anamnesis='%4', bad_habits='%5', family_anamnesis='%6', allergic_anamnesis='%7' WHERE id=%8").arg(data.at(0)).arg(data.at(1)).arg(data.at(2)).arg(data.at(3)).arg(data.at(4)).arg(data.at(5)).arg(data.at(6)).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setEpicrisisData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET epicrisis='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setFinishDiagnosisData(QStringList data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET finish_diagnosis_bases='%1', finish_diagnosis='%2' WHERE id=%3").arg(data.at(0)).arg(data.at(1)).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setLaboratoryResearchData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET laboratory_research='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setMedicalHisroryData(QStringList data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET ill_complaints='%1', anamnesis_sickness='%2' WHERE id=%3").arg(data.at(0)).arg(data.at(1)).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setObjectiveStatusData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET objective_status='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setOrganSystemsData(QStringList data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET respiration_organs='%1', digestion_organs='%2', urination_organs='%3' WHERE id=%4").arg(data.at(0)).arg(data.at(1)).arg(data.at(2)).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setPasportData(QList< QVariant > data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET fio='%1', dob='%2', place_empl='%3', delivery_date='%4', assign='%5', diagnosis='%6', bedroom_diagnosis='%7', address='%8' WHERE id=%9").arg(data.at(0).toString()).arg(data.at(1).toDate().toString("yyyy-MM-dd")).arg(data.at(2).toString()).arg(data.at(3).toDate().toString("yyyy-MM-dd")).arg(data.at(4).toString()).arg(data.at(5).toString()).arg(data.at(6).toString()).arg(data.at(7).toString()).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setPilotDiagnosisData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET pilot_diagnosis='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setPrognosisData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET prognosis='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setProphylaxisData(const QString &data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET prophylaxis='%1' WHERE id=%2").arg(data).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

bool DataHistoryManager::setTreatmentData(QStringList data)
{
    bool ok = query->exec(QString("UPDATE case_histories SET regimen='%1', diet='%2', medicament_treatment='%3', physiotherapy='%4', physiotherapy_exercises='%5', sanatoria='%6', operative='%7', dispensary='%8' WHERE id=%9").arg(data.at(0)).arg(data.at(1)).arg(data.at(2)).arg(data.at(3)).arg(data.at(4)).arg(data.at(5)).arg(data.at(6)).arg(data.at(7)).arg(historyId));

    if(!ok)
        lastErrorString = query->lastError().text();

    return ok;
}

QString DataHistoryManager::getLastErrorString() const
{
    return lastErrorString;
}

#include "datahistorymanager.moc"
