#ifndef edumis_H
#define edumis_H

#include <QtGui/QMainWindow>
#include "ui_centerwindow.h"

class SetupDialog;
class DataManager;
class HisWindow;
class LoginDlg;
class QSettings;

namespace Ui
{
    class CenterWindow;
};

class edumis : public QMainWindow
{
    Q_OBJECT
public:
    explicit edumis(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    virtual ~edumis();

    void createList();

protected:
    void showEvent(QShowEvent *event);
    void closeEvent(QCloseEvent *event);

public slots:
    void on_listWidget_itemClicked(QListWidgetItem *current);

private:
    Ui::CenterWindow *ui;
    LoginDlg *lDlg;
    SetupDialog *setupDialog;
    HisWindow *historyWindow;
    DataManager *dataManager;
    QSettings *settings;
    QString dbFileName;
    
    void readSettings();
    void writeSettings();
    void getDbFileName();
    void openDataBase();
};

#endif // edumis_H
