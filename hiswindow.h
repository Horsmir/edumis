/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HISWINDOW_H
#define HISWINDOW_H

#include <QtGui/QMainWindow>
#include "ui_historywindow.h"

class OpenHistoryDialog;
class QSqlDatabase;
class DataHistoryManager;

namespace Ui
{
    class HistoryWindow;
};

class HisWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit HisWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    virtual ~HisWindow();

    void setDb(QSqlDatabase *db);
    void setUserId(quint32 userId);

    void viewPasportData();
    void viewMedicalHisroryData(); // отобразить анамнез заболевания
    void viewAnamnesisLifeData(); // отобразить анамнез жизни
    void viewObjectiveStatusData(); // отобразить объективный статус
    void viewOrganSystemsData(); // отобразить описание состояния систем органов
    void viewPilotDiagnosisData(); // отобразить предварительный диагноз и его обоснование
    void viewLaboratoryResearchData(); // отобразить лабораторные данные
    void viewFinishDiagnosisData(); // отобразить окончательный диагноз и его обоснование
    void viewTreatmentData(); // отобразить лечение
    void viewProphylaxisData(); // отобразить профилактику
    void viewPrognosisData(); // отобразить прогноз и его обоснование
    void viewEpicrisisData(); // отобразить эпикриз

    void savePasportData(); // сохранить паспортные данные
    void saveMedicalHisroryData(); // сохранить анамнез заболевания
    void saveAnamnesisLifeData(); // сохранить анамнез жизни
    void saveObjectiveStatusData(); // сохранить объективный статус
    void saveOrganSystemsData(); // сохранить описание состояния систем органов
    void savePilotDiagnosisData(); // сохранить предварительный диагноз и его обоснование
    void saveLaboratoryResearchData(); // сохранить лабораторные данные
    void saveFinishDiagnosisData(); // сохранить окончательный диагноз и его обоснование
    void saveTreatmentData(); // сохранить лечение
    void saveProphylaxisData(); // сохранить профилактику
    void savePrognosisData(); // сохранить прогноз и его обоснование
    void saveEpicrisisData(); // сохранить эпикриз

    void saveData();

public slots:
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_deBurthday_dateChanged(const QDate &date);
    void on_toolBox_currentChanged(int index);
    void on_actionSave_triggered();
    void on_actionAbout_triggered();

private:
    Ui::HistoryWindow *ui;
    DataHistoryManager *dataHistoryManager;
    OpenHistoryDialog *openHistoryDlg;
    qint32 age(QDate dBirth);
    qint32 currentIndexTab;
    bool tableOpened;
};

#endif // HISWINDOW_H
