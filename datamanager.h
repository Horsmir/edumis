/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>


class DataManager : public QObject
{
    Q_OBJECT
public:
    explicit DataManager(QObject *parent = 0);
    virtual ~DataManager();

    bool connectDb(const QString &databaseName);
    QString getLastError() const;
    bool checkUser(const QString &userName, const QString &password);
    void disconnectDb();

    QSqlDatabase *getDatabase();
    quint32 getUserId() const;

private:
    QSqlDatabase db;
    QString lastError;
    quint32 userId;
};

#endif // DATAMANAGER_H
