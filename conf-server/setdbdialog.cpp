/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include "setdbdialog.h"
#include "dataconfmanager.h"

SetDbDialog::SetDbDialog(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::SetDbDlg), dbFileName(QString())
{
    ui->setupUi(this);
}

SetDbDialog::~SetDbDialog()
{
    delete ui;
}

QString SetDbDialog::getDbFileName() const
{
    return dbFileName;
}

void SetDbDialog::on_createButton_clicked()
{
    QFileDialog::Options options;
    options |= QFileDialog::DontUseNativeDialog;
    QString selectedFilter;
    dbFileName = QFileDialog::getSaveFileName(this, trUtf8("Создание базы данных"), QDir::homePath(), trUtf8("Все файлы (*);;Файлы SQLite (*.sqlite)"), &selectedFilter, options);
    if(!dbFileName.isEmpty())
    {
	if(!dataConfManager->createDb(dbFileName))
	    {
		QMessageBox::critical(this, trUtf8("Соединение с базой данных"), trUtf8("%1").arg(dataConfManager->getLastError()));
	    }
    }
    accept();
}

void SetDbDialog::on_openButton_clicked()
{
    QFileDialog::Options options;
    options |= QFileDialog::DontUseNativeDialog;
    QString selectedFilter;
    dbFileName = QFileDialog::getOpenFileName(this, trUtf8("Выбор файла базы данных"), QDir::homePath(), trUtf8("Все файлы (*);;Файлы SQLite (*.sqlite)"), &selectedFilter, options);
    if(!dbFileName.isEmpty())
    {
	if(!dataConfManager->connectDb(dbFileName))
	{
	    QMessageBox::critical(this, trUtf8("Соединение с базой данных"), trUtf8("%1").arg(dataConfManager->getLastError()));
	}
    }
    accept();
}

void SetDbDialog::setDataConfManager(DataConfManager *dm)
{
    dataConfManager = dm;
}

#include "setdbdialog.moc"
