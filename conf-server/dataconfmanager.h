/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DATACONFMANAGER_H
#define DATACONFMANAGER_H

#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtCore/QVariantList>

class QSqlQuery;

class DataConfManager : public QObject
{
    Q_OBJECT
public:
    explicit DataConfManager(QObject *parent = 0);
    virtual ~DataConfManager();

    bool connectDb(const QString &databaseName);
    QString getLastError() const;
    void disconnectDb();
    bool createDb(const QString &databaseName);

    QList< QStringList > getData() const;
    bool addUser(const QString &userName, const QString &userLogin, const QString &userPassword, const QString &userGroup);
    bool editUser(quint32 userId, const QString &userName, const QString &userLogin, const QString &userPassword, const QString &userGroup);
    bool delUser(quint32 userId);
    QString md5(const QString &str);

private:
    QSqlDatabase db;
    QString lastError;
    QSqlQuery *query;
};

#endif // DATACONFMANAGER_H
