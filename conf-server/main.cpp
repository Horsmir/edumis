#include <QtGui/QApplication>
#include <QtCore/QTranslator>
#include "confedumis.h"


int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    
    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(), "/usr/share/qt4/translations");
    app.installTranslator(&qtTranslator);
    
    ConfEduMis confmis;
    confmis.show();
    return app.exec();
}
