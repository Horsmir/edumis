/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtCore/QDebug>
#include <QtGui/QMessageBox>
#include <QtGui/QShowEvent>
#include <QtCore/QSettings>
#include "confedumis.h"
#include "dataconfmanager.h"
#include "adduserdialog.h"
#include "setdbdialog.h"

ConfEduMis::ConfEduMis(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::AdvanceUsersDialog), dbFileName(QString())
{
    ui->setupUi(this);
    dataConfManager = new DataConfManager(this);
    
    settings = new QSettings("../../config/confedumis.conf", QSettings::NativeFormat, this); // TODO: заменить на: QSettings(QSettings::NativeFormat, QSettings::UserScope, "EduMIS", "confedumis", this)
    readSettings();

    if(dbFileName.isEmpty())
    {
	setDbDialog = new SetDbDialog(this);
	setDbDialog->setDataConfManager(dataConfManager);
	setDbDialog->exec();
	dbFileName = setDbDialog->getDbFileName();
	if(dbFileName.isEmpty())
	{
	    QMessageBox::critical(this, trUtf8("Соединение с базой данных"), trUtf8("Файл базы данных не задан."));
	}
    }
    else
    {
	if(!dataConfManager->connectDb(dbFileName))
	{
	    QMessageBox::critical(this, trUtf8("Соединение с базой данных"), trUtf8("%1").arg(dataConfManager->getLastError()));
	}
    }
    
    showData();
        
    addUserDlg = new AddUserDialog(this);
}

ConfEduMis::~ConfEduMis()
{
    delete ui;
}

void ConfEduMis::showData()
{
    QList<QStringList> res = dataConfManager->getData();

    ui->tableWidget->setRowCount(res.count());

    for(quint32 i = 0; i < res.count(); i++)
    {
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(res.at(i).at(0)));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(res.at(i).at(1)));
        ui->tableWidget->setItem(i, 2, new QTableWidgetItem(res.at(i).at(2)));
        ui->tableWidget->setItem(i, 3, new QTableWidgetItem(res.at(i).at(3)));
    }
}

void ConfEduMis::on_addButton_clicked()
{
    if(addUserDlg->exec() == QDialog::Accepted)
    {
        bool ok = dataConfManager->addUser(addUserDlg->getUserName(), addUserDlg->getUserLogin(), addUserDlg->getUserPassword(), addUserDlg->getUserGroup());

        if(!ok)
            QMessageBox::critical(this, trUtf8("Добавление пользователя"), trUtf8("Не возможно добавить пользователя.\n%1").arg(dataConfManager->getLastError()));

        showData();
    }
}

void ConfEduMis::on_delButton_clicked()
{
    qint32 id = ui->tableWidget->currentRow();

    if(id < 0)
    {
        QMessageBox::information(this, trUtf8("Удаление пользователя"), trUtf8("Выберите пользователя для удаления."));
        return;
    }

    bool ok = dataConfManager->delUser(id);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Удаление пользователя"), trUtf8("Не возможно удалить пользователя.\n%1").arg(dataConfManager->getLastError()));

    showData();
}

void ConfEduMis::on_editButton_clicked()
{
    qint32 id = ui->tableWidget->currentRow();

    if(id < 0)
    {
        QMessageBox::information(this, trUtf8("Изменение пользователя"), trUtf8("Выберите пользователя для изменения."));
        return;
    }

    addUserDlg->setUserName(ui->tableWidget->item(id, 1)->text());
    addUserDlg->setUserLogin(ui->tableWidget->item(id, 2)->text());
    addUserDlg->setUserGroup(ui->tableWidget->item(id, 3)->text());

    if(addUserDlg->exec() == QDialog::Accepted)
    {
        bool ok = dataConfManager->editUser(ui->tableWidget->item(id, 0)->text().toUInt(), addUserDlg->getUserName(), addUserDlg->getUserLogin(), addUserDlg->getUserPassword(), addUserDlg->getUserGroup());

        if(!ok)
            QMessageBox::critical(this, trUtf8("Изменение пользователя"), trUtf8("Не возможно изменить пользователя.\n%1").arg(dataConfManager->getLastError()));

        showData();
    }

}

void ConfEduMis::readSettings()
{
    dbFileName = settings->value("General/DataBaseFileName", QString()).toString();
}

void ConfEduMis::writeSettings()
{
    settings->setValue("General/DataBaseFileName", dbFileName);
}

void ConfEduMis::closeEvent(QCloseEvent *event)
{
    writeSettings();
    QDialog::closeEvent(event);
}

#include "confedumis.moc"
