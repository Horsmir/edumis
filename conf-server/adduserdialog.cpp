/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::AddUserDlg)
{
    ui->setupUi(this);
}

AddUserDialog::~AddUserDialog()
{
    delete ui;
}

QString AddUserDialog::getUserGroup() const
{
    return ui->leGroup->text();
}

QString AddUserDialog::getUserLogin() const
{
    return ui->leLogin->text();
}

QString AddUserDialog::getUserName() const
{
    return ui->leName->text();
}

QString AddUserDialog::getUserPassword() const
{
    return ui->lePassword->text();
}

void AddUserDialog::setUserGroup(const QString &userGroup)
{
    ui->leGroup->clear();
    ui->leGroup->setText(userGroup);
}

void AddUserDialog::setUserLogin(const QString &userLogin)
{
    ui->leLogin->clear();
    ui->leLogin->setText(userLogin);
}

void AddUserDialog::setUserName(const QString &userName)
{
    ui->leName->clear();
    ui->lePassword->clear();
    ui->leName->setText(userName);
}

#include "adduserdialog.moc"
