/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONFEDUMIS_H
#define CONFEDUMIS_H

#include <QtGui/QDialog>
#include "ui_advanceusersdlg.h"

class QSettings;
class SetDbDialog;
class AddUserDialog;
class DataConfManager;

namespace Ui
{
    class AdvanceUsersDialog;
}

class ConfEduMis : public QDialog
{
    Q_OBJECT
public:
    explicit ConfEduMis(QWidget *parent = 0, Qt::WindowFlags f = 0);
    virtual ~ConfEduMis();

    void showData();
    
protected:
    void closeEvent(QCloseEvent *event);

public slots:
    void on_addButton_clicked();
    void on_editButton_clicked();
    void on_delButton_clicked();

private:
    Ui::AdvanceUsersDialog *ui;
    DataConfManager *dataConfManager;
    AddUserDialog *addUserDlg;
    SetDbDialog *setDbDialog;
    QSettings *settings;
    QString dbFileName;
    
    void readSettings();
    void writeSettings();
};

#endif // CONFEDUMIS_H
