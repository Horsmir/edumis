/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtCore/QStringList>
#include <QtCore/QCryptographicHash>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include "dataconfmanager.h"
#include "edumisconfigure.h"

DataConfManager::DataConfManager(QObject *parent): QObject(parent), lastError(QString())
{

}

DataConfManager::~DataConfManager()
{
    delete query;
}

bool DataConfManager::connectDb(const QString &databaseName)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(databaseName);

    bool ok = db.open();
    query = new QSqlQuery(db);

    if(!ok)
        lastError = db.lastError().text();

    return ok;
}

void DataConfManager::disconnectDb()
{
    db.close();
}

QString DataConfManager::getLastError() const
{
    return lastError;
}

QList<QStringList> DataConfManager::getData() const
{
    QList<QStringList> res;

    query->exec("SELECT id, name, login, user_group FROM users");

    while(query->next())
    {
        QStringList temp;
        temp.append(query->value(0).toString());
        temp.append(query->value(1).toString());
        temp.append(query->value(2).toString());
        temp.append(query->value(3).toString());

        res.append(temp);
    }

    return res;
}

bool DataConfManager::addUser(const QString &userName, const QString &userLogin, const QString &userPassword, const QString &userGroup)
{
    bool ok = query->exec(QString("INSERT INTO users(name, login, password, user_group) VALUES ('%1', '%2', '%3', '%4')").arg(userName).arg(userLogin).arg(md5(userPassword)).arg(userGroup));

    if(!ok)
        lastError = query->lastError().text();

    return ok;
}

bool DataConfManager::delUser(quint32 userId)
{
    bool ok = query->exec(QString("DELETE FROM users WHERE id=%1").arg(userId));

    if(!ok)
        lastError = query->lastError().text();

    return ok;
}

bool DataConfManager::editUser(quint32 userId, const QString &userName, const QString &userLogin, const QString &userPassword, const QString &userGroup)
{
    bool ok = query->exec(QString("UPDATE users SET name='%1', login='%2', password='%3', user_group='%4' WHERE id=%5").arg(userName).arg(userLogin).arg(md5(userPassword)).arg(userGroup).arg(userId));

    if(!ok)
        lastError = query->lastError().text();

    return ok;
}

QString DataConfManager::md5(const QString &str)
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(str.toAscii());
    return (hash.result().toHex());
}

bool DataConfManager::createDb(const QString &databaseName)
{
    QFile sqlFile("../../data/edumisdb.sql"); // TODO: Заменить на DATA_PATH + "/sql/edumis.sql"
    if(!sqlFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
	lastError = trUtf8("Не возможно открыть файл: \"%1\"").arg(sqlFile.fileName());
	return false;
    }
    
    connectDb(databaseName);
    
    QTextStream in(&sqlFile);
    while(!in.atEnd())
    {
	QString line = in.readLine();
	if(!query->exec(line))
	{
	    lastError = query->lastError().text();
	    return false;
	}
    }
    return true;
}

#include "dataconfmanager.moc"
