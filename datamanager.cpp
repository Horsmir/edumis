/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtCore/QDebug>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtCore/QCryptographicHash>
#include <QtCore/QVariant>
#include "datamanager.h"

DataManager::DataManager(QObject *parent): QObject(parent), userId(0)
{
    lastError = QString();
}

DataManager::~DataManager()
{

}

bool DataManager::connectDb(const QString &databaseName)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(databaseName);
    bool ok = db.open();

    if(!ok)
        lastError = db.lastError().text();

    return ok;
}

bool DataManager::checkUser(const QString &userName, const QString &password)
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(password.toAscii());
    QString hashPassword(hash.result().toHex());

    QSqlQuery query;
    query.exec(QString("SELECT id, password FROM users WHERE login='%1'").arg(userName));

    if(query.next())
    {
        QString pass = query.value(1).toString();

        if(hashPassword == pass)
        {
            userId = query.value(0).toUInt();
            return true;
        }
    }

    return false;
}

QString DataManager::getLastError() const
{
    return lastError;
}

void DataManager::disconnectDb()
{
    db.close();
}

QSqlDatabase *DataManager::getDatabase()
{
    return &db;
}

quint32 DataManager::getUserId() const
{
    return userId;
}

#include "datamanager.moc"
