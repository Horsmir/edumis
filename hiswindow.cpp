/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtGui/QMessageBox>
#include "hiswindow.h"
#include "datahistorymanager.h"
#include "openhistorydialog.h"
#include "edumisconfigure.h"

HisWindow::HisWindow(QWidget *parent, Qt::WindowFlags flags): QMainWindow(parent, flags), ui(new Ui::HistoryWindow), tableOpened(false)
{
    ui->setupUi(this);
    ui->toolBox->setEnabled(false);
    ui->deInDate->setDate(QDate::currentDate());
    dataHistoryManager = new DataHistoryManager(this);
    openHistoryDlg = new OpenHistoryDialog(this);

    currentIndexTab = 0;
    ui->toolBox->setCurrentIndex(currentIndexTab);
    
    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

HisWindow::~HisWindow()
{
    delete ui;
}

void HisWindow::setDb(QSqlDatabase *db)
{
    dataHistoryManager->setDatabase(db);
}

void HisWindow::setUserId(quint32 userId)
{
    dataHistoryManager->setUserId(userId);
}

void HisWindow::on_actionNew_triggered()
{
    if(dataHistoryManager->addNewHistory())
    {
        ui->toolBox->setEnabled(true);
        tableOpened = true;
        viewPasportData();
    }
    else
    {
        //TODO: MessageBox(Error)
    }
}

void HisWindow::on_actionOpen_triggered()
{
    openHistoryDlg->setHistories(dataHistoryManager->getHistoryList());

    if(openHistoryDlg->exec() == QDialog::Accepted)
    {
        dataHistoryManager->setHistoryId(openHistoryDlg->getHistoryId());
        ui->toolBox->setEnabled(true);
        tableOpened = true;
        viewPasportData();
    }
}

void HisWindow::viewPasportData()
{
    QList<QVariant> res = dataHistoryManager->getPasportData();

    ui->leFIO->setText(res.at(0).toString());
    QDate dBirth = res.at(1).toDate();
    ui->deBurthday->setDate(dBirth);
    ui->labelAge->setNum(age(dBirth));
    ui->pteAddress->document()->setPlainText(res.at(7).toString());
    ui->leWork->setText(res.at(2).toString());
    ui->deInDate->setDate(res.at(3).toDate());
    ui->leWhoIn->setText(res.at(4).toString());
    ui->leInDiagnos->setText(res.at(5).toString());
    ui->leClinicDiagnos->setText(res.at(6).toString());
}

qint32 HisWindow::age(QDate dBirth)
{
    QDate today = QDate::currentDate();
    qint32 years = today.year() - dBirth.year();

    if(today.month() < dBirth.month())
        years--;

    if(today.month() == dBirth.month())
        if(today.day() < dBirth.day())
            years--;

    return years;
}

void HisWindow::on_deBurthday_dateChanged(const QDate &date)
{
    ui->labelAge->setNum(age(date));
}

void HisWindow::saveAnamnesisLifeData()
{
    QStringList res;

    res.append(ui->pteBiography->document()->toPlainText());
    res.append(ui->pteLabourAnamnesis->document()->toPlainText());
    res.append(ui->pteEte->document()->toPlainText());
    res.append(ui->pteEpidemiologicalAnamnesis->document()->toPlainText());
    res.append(ui->pteBadHabits->document()->toPlainText());
    res.append(ui->pteFamilyAnamnesis->document()->toPlainText());
    res.append(ui->pteAllergicAnamnesis->document()->toPlainText());

    bool ok = dataHistoryManager->setAnamnesisLifeData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить анамнез жизни.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveEpicrisisData()
{
    bool ok = dataHistoryManager->setEpicrisisData(ui->pteEpicrisis->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить эпикриз.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveFinishDiagnosisData()
{
    QStringList res;

    res.append(ui->pteFinishDiagnosisBases->document()->toPlainText());
    res.append(ui->pteFinishDiagnosis->document()->toPlainText());

    bool ok = dataHistoryManager->setFinishDiagnosisData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить окончательный диагноз.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveLaboratoryResearchData()
{
    bool ok = dataHistoryManager->setLaboratoryResearchData(ui->pteLaboratoryResearch->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить лабораторные данные.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveMedicalHisroryData()
{
    QStringList res;

    res.append(ui->ptePatientComplaint->document()->toPlainText());
    res.append(ui->pteMedicalHisrory->document()->toPlainText());

    bool ok = dataHistoryManager->setMedicalHisroryData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить анамнез заболевания.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveObjectiveStatusData()
{
    bool ok = dataHistoryManager->setObjectiveStatusData(ui->pteObjectiveStatus->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить объективный статус.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveOrganSystemsData()
{
    QStringList res;

    res.append(ui->pteRespirationOrgans->document()->toPlainText());
    res.append(ui->pteDigestionOrgans->document()->toPlainText());
    res.append(ui->pteUrinationOrgans->document()->toPlainText());

    bool ok = dataHistoryManager->setOrganSystemsData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить опрос по системам и органам.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::savePasportData()
{
    QList<QVariant> res;

    res.append(QVariant(ui->leFIO->text()));
    res.append(QVariant(ui->deBurthday->date()));
    res.append(QVariant(ui->leWork->text()));
    res.append(QVariant(ui->deInDate->date()));
    res.append(QVariant(ui->leWhoIn->text()));
    res.append(QVariant(ui->leInDiagnos->text()));
    res.append(QVariant(ui->leClinicDiagnos->text()));
    res.append(QVariant(ui->pteAddress->document()->toPlainText()));

    bool ok = dataHistoryManager->setPasportData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить паспортные данные.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::savePilotDiagnosisData()
{
    bool ok = dataHistoryManager->setPilotDiagnosisData(ui->ptePilotDiagnosis->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить предварительный диагноз и его обоснование.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::savePrognosisData()
{
    bool ok = dataHistoryManager->setPrognosisData(ui->ptePrognosis->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить прогноз и его обоснование.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveProphylaxisData()
{
    bool ok = dataHistoryManager->setProphylaxisData(ui->pteProphylaxis->document()->toPlainText());

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить профилактику.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::saveTreatmentData()
{
    QStringList res;

    res.append(ui->pteRegimen->document()->toPlainText());
    res.append(ui->pteDiet->document()->toPlainText());
    res.append(ui->pteMedicamentTreatment->document()->toPlainText());
    res.append(ui->ptePhysiotherapy->document()->toPlainText());
    res.append(ui->ptePhysiotherapyExercises->document()->toPlainText());
    res.append(ui->pteSanatoria->document()->toPlainText());
    res.append(ui->pteOperative->document()->toPlainText());
    res.append(ui->pteDispensary->document()->toPlainText());

    bool ok = dataHistoryManager->setTreatmentData(res);

    if(!ok)
        QMessageBox::critical(this, trUtf8("Сохранение базы данных"), trUtf8("Не возможно сохранить лечение.\n%1").arg(dataHistoryManager->getLastErrorString()));
}

void HisWindow::viewAnamnesisLifeData()
{
    QStringList res = dataHistoryManager->getAnamnesisLifeData();

    ui->pteBiography->clear();
    ui->pteLabourAnamnesis->clear();
    ui->pteEte->clear();
    ui->pteEpidemiologicalAnamnesis->clear();
    ui->pteBadHabits->clear();
    ui->pteFamilyAnamnesis->clear();
    ui->pteAllergicAnamnesis->clear();

    ui->pteBiography->document()->setPlainText(res.at(0));
    ui->pteLabourAnamnesis->document()->setPlainText(res.at(1));
    ui->pteEte->document()->setPlainText(res.at(2));
    ui->pteEpidemiologicalAnamnesis->document()->setPlainText(res.at(3));
    ui->pteBadHabits->document()->setPlainText(res.at(4));
    ui->pteFamilyAnamnesis->document()->setPlainText(res.at(5));
    ui->pteAllergicAnamnesis->document()->setPlainText(res.at(6));
}

void HisWindow::viewEpicrisisData()
{
    ui->pteEpicrisis->clear();
    ui->pteEpicrisis->document()->setPlainText(dataHistoryManager->getEpicrisisData());
}

void HisWindow::viewFinishDiagnosisData()
{
    QStringList res = dataHistoryManager->getFinishDiagnosisData();

    ui->pteFinishDiagnosisBases->clear();
    ui->pteFinishDiagnosis->clear();

    ui->pteFinishDiagnosisBases->document()->setPlainText(res.at(0));
    ui->pteFinishDiagnosis->document()->setPlainText(res.at(1));
}

void HisWindow::viewLaboratoryResearchData()
{
    ui->pteLaboratoryResearch->clear();
    ui->pteLaboratoryResearch->document()->setPlainText(dataHistoryManager->getLaboratoryResearchData());
}

void HisWindow::viewMedicalHisroryData()
{
    QStringList res = dataHistoryManager->getMedicalHisroryData();

    ui->ptePatientComplaint->clear();
    ui->pteMedicalHisrory->clear();

    ui->ptePatientComplaint->document()->setPlainText(res.at(0));
    ui->pteMedicalHisrory->document()->setPlainText(res.at(1));
}

void HisWindow::viewObjectiveStatusData()
{
    ui->pteObjectiveStatus->clear();
    ui->pteObjectiveStatus->document()->setPlainText(dataHistoryManager->getObjectiveStatusData());
}

void HisWindow::viewOrganSystemsData()
{
    QStringList res = dataHistoryManager->getOrganSystemsData();

    ui->pteRespirationOrgans->clear();
    ui->pteDigestionOrgans->clear();
    ui->pteUrinationOrgans->clear();

    ui->pteRespirationOrgans->document()->setPlainText(res.at(0));
    ui->pteDigestionOrgans->document()->setPlainText(res.at(1));
    ui->pteUrinationOrgans->document()->setPlainText(res.at(2));
}

void HisWindow::viewPilotDiagnosisData()
{
    ui->ptePilotDiagnosis->clear();
    ui->ptePilotDiagnosis->document()->setPlainText(dataHistoryManager->getPilotDiagnosisData());
}

void HisWindow::viewPrognosisData()
{
    ui->ptePrognosis->clear();
    ui->ptePrognosis->document()->setPlainText(dataHistoryManager->getPrognosisData());
}

void HisWindow::viewProphylaxisData()
{
    ui->pteProphylaxis->clear();
    ui->pteProphylaxis->document()->setPlainText(dataHistoryManager->getProphylaxisData());
}

void HisWindow::viewTreatmentData()
{
    QStringList res = dataHistoryManager->getTreatmentData();

    ui->pteRegimen->clear();
    ui->pteDiet->clear();
    ui->pteMedicamentTreatment->clear();
    ui->ptePhysiotherapy->clear();
    ui->ptePhysiotherapyExercises->clear();
    ui->pteSanatoria->clear();
    ui->pteOperative->clear();
    ui->pteDispensary->clear();

    ui->pteRegimen->document()->setPlainText(res.at(0));
    ui->pteDiet->document()->setPlainText(res.at(1));
    ui->pteMedicamentTreatment->document()->setPlainText(res.at(2));
    ui->ptePhysiotherapy->document()->setPlainText(res.at(3));
    ui->ptePhysiotherapyExercises->document()->setPlainText(res.at(4));
    ui->pteSanatoria->document()->setPlainText(res.at(5));
    ui->pteOperative->document()->setPlainText(res.at(6));
    ui->pteDispensary->document()->setPlainText(res.at(7));
}

void HisWindow::saveData()
{
    switch(currentIndexTab)
    {
    case 0:
        savePasportData();
        break;
    case 1:
        saveMedicalHisroryData();
        break;
    case 2:
        saveAnamnesisLifeData();
        break;
    case 3:
        saveObjectiveStatusData();
        break;
    case 4:
        saveOrganSystemsData();
        break;
    case 5:
        savePilotDiagnosisData();
        break;
    case 6:
        saveLaboratoryResearchData();
        break;
    case 7:
        saveFinishDiagnosisData();
        break;
    case 8:
        saveTreatmentData();
        break;
    case 9:
        saveProphylaxisData();
        break;
    case 10:
        savePrognosisData();
        break;
    case 11:
        saveEpicrisisData();
        break;
    default:
        break;
    }
}

void HisWindow::on_toolBox_currentChanged(int index)
{
    if(tableOpened)
    {
        saveData();
        currentIndexTab = index;

        switch(currentIndexTab)
        {
        case 0:
            viewPasportData();
            break;
        case 1:
            viewMedicalHisroryData();
            break;
        case 2:
            viewAnamnesisLifeData();
            break;
        case 3:
            viewObjectiveStatusData();
            break;
        case 4:
            viewOrganSystemsData();
            break;
        case 5:
            viewPilotDiagnosisData();
            break;
        case 6:
            viewLaboratoryResearchData();
            break;
        case 7:
            viewFinishDiagnosisData();
            break;
        case 8:
            viewTreatmentData();
            break;
        case 9:
            viewProphylaxisData();
            break;
        case 10:
            viewPrognosisData();
            break;
        case 11:
            viewEpicrisisData();
            break;
        default:
            break;
        }
    }
}

void HisWindow::on_actionSave_triggered()
{
    saveData();
}

void HisWindow::on_actionAbout_triggered()
{
    QString str1, str2, str3, str4;

    str1 = trUtf8("<h2>%1-%2</h2><p><b>%1</b>  - Учебная медицинская информационная система. Предназначена для обретения практических навыков студентами медицинских институтов и колледжей в обработке медицинской информации. Написана на Qt-C++.</p><p>Copyright &copy;  2013 Роман Браун</p><p>Icons: <a href=\"http://www.smashingmagazine.com/2010/02/15/free-medical-icons-set-60-icons/\">www.smashingmagazine.com</a></p>").arg(APP_NAME).arg(VERSION);
    str2 = trUtf8("<p>Это программа является свободным программным обеспечением. Вы можете распространять и/или модифицировать её согласно условиям Стандартной Общественной Лицензии GNU, опубликованной Фондом Свободного Программного Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии.</p>");
    str3 = trUtf8("<p>Эта программа распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную Общественную Лицензию GNU для получения дополнительной информации.</p>");
    str4 = trUtf8("<p>Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе с программой. В случае её отсутствия, посмотрите <a href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</a>.</p><p>E-Mail: <a href=\"mailto:firdragon76@gmail.com\">firdragon76@gmail.com</a><br>Сайт программы: <a href=\"bitbucket.org/Horsmir/edumis\">bitbucket.org/Horsmir/edumis</a></p>");

    QMessageBox::about(this, trUtf8("О программе"), str1 + str2 + str3 + str4);
}

#include "hiswindow.moc"
